import React from 'react';
import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import cx from 'classnames';

import styles from './App.module.css';
import config from './config/firebaseConfig';

class App extends React.Component {
    constructor(props) {
        super(props);

        if(!app.apps.length) {
            app.initializeApp(config);
        }

        this.auth = app.auth();
        this.db = app.firestore();

        this.state = {
            posts: [],
            title: '',
            body: ''
        }
    }

    componentDidMount() {
        this.db
            .collection('posts')
            .get()
            .then(snapshot => {
                const posts = [];

                snapshot.forEach(snap => {
                    posts.push({
                        id: snap.id,
                        ...snap.data()
                    })
                })

                this.setState({ posts })
            })
    }

    handleSubmit = event => {
        event.preventDefault();

        const { title, body } = this.state;
        const post = { title, body }

        if(!title) {
            return;
        }

        const ref = this.db.collection('posts').doc();

        ref.set(post).then(() => {
            this.setState(({ posts }) => ({
                posts: [{ ...post, id: ref.id }, ...posts]
            }))
        })
    }

    handleChange = ({ target: { name, value }}) => {
        this.setState({ [name]: value });
    }

    render() {
        return (
            <div className={cx('container', styles.root)}>
                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <div className={cx('card', styles.form)}>
                            <div className="card-body">
                                <form className={styles.form} onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="post-title">Post title</label>
                                        <input 
                                        onChange={this.handleChange} 
                                        value={this.state.title} 
                                        type="text" 
                                        name="title"
                                        className="form-control" 
                                        id="post-title" 
                                        aria-describedby="Post title" />
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="post-body">Post body</label>
                                        <input 
                                        onChange={this.handleChange} 
                                        value={this.state.body} 
                                        type="text" 
                                        name="body"
                                        className="form-control" 
                                        id="post-body" 
                                        aria-describedby="Post body" />
                                    </div>

                                    <button className="btn btn-primary" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-6 offset-lg-3">
                        <div className="posts">
                            {
                                this.state.posts.map(post => {
                                    return <div key={post.id} className={cx('card', styles.post)}>
                                        <div className="card-body">
                                            <h4 className="card-title">{post.title}</h4>
                                            <p className="card-text">{post.body}</p>
                                        </div>
                                    </div>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
